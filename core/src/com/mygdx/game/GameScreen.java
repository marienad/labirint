package com.mygdx.game;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;

public class GameScreen implements Screen {
    private static final int WIDTH = 500;
    private static final int CAMERA_HEIGHT = 300;
    private static final int HEIGHT = 650;
    private static final int SPEED = 60;
    private final Labirint game;
    private static final int SIZE = 40;
    private OrthographicCamera camera;
    private SpriteBatch batch;
    private Texture cubeImg;
    private Music music;
    private Texture wallImg;
    private Texture emptyImg;
    private Rectangle cube;
    private Rectangle finish;
    private Rectangle light;
    private Texture lightImg;
    private Cell[][] map;
    private ArrayList<Cell> walls;
    private int[][] maze;

    GameScreen(final Labirint gam) {
        this.game = gam;
        camera = new OrthographicCamera(WIDTH, HEIGHT);
        camera.setToOrtho(false, WIDTH, CAMERA_HEIGHT);

        batch = new SpriteBatch();
        cubeImg = new Texture("cube.png");
        wallImg = new Texture("wall.png");
        emptyImg = new Texture("empty.png");
        lightImg = new Texture("light.png");
        music = Gdx.audio.newMusic(Gdx.files.internal("atmosphere.mp3"));
        music.setLooping(true);
        maze = (new MazeGenerator()).getMaze();
        map = new Cell[maze.length][maze[0].length];
        walls = new ArrayList<>();
        fillMap();
    }

    private void limits() {
        if (cube.x < 0) {
            cube.x = 0;
        }
        if (cube.y < 0) {
            cube.y = 0;
        }
        if (cube.x > WIDTH - SIZE) {
            cube.x = WIDTH - SIZE;
        }
        if (camera.position.y / 2 < cube.y) {
            if (cube.y < CAMERA_HEIGHT / 2) {
                camera.translate(0, 0);
                return;
            }
            float a = cube.y - camera.position.y;
            camera.translate(0, a);
        }
        if (cube.y > HEIGHT) {
            cube.y = HEIGHT;
        }
    }

    private void inputKey() {
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            if (isNotWall(cube.x, cube.y) && isNotWall(cube.x, cube.y + SIZE))
                cube.x -= SPEED * Gdx.graphics.getDeltaTime();
        }
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            if (isNotWall(cube.x + SIZE, cube.y) && isNotWall(cube.x + SIZE, cube.y + SIZE))
                cube.x += SPEED * Gdx.graphics.getDeltaTime();

        }
        if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            if (isNotWall(cube.x, cube.y) && isNotWall(cube.x + SIZE, cube.y))
                cube.y -= SPEED * Gdx.graphics.getDeltaTime();

        }
        if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
            if (isNotWall(cube.x, cube.y + SIZE) && isNotWall(cube.x + SIZE, cube.y + SIZE))
                cube.y += SPEED * Gdx.graphics.getDeltaTime();
        }
    }

    private void InputAn() {
        if (Gdx.input.isTouched()) {
            float x = Gdx.input.getX();
            float y = Gdx.input.getY();
            Vector3 vector3 = new Vector3(x, y, 0);
            camera.unproject(vector3);
            x = vector3.x;
            y = vector3.y;
            if (x >= cube.x - 10 && x <= cube.x + SIZE + 10) {
                if (y > cube.y) {
                    if (isNotWall(cube.x, cube.y + SIZE) && isNotWall(cube.x + SIZE, cube.y + SIZE))
                        cube.y += SPEED * Gdx.graphics.getDeltaTime();
                } else {
                    if (isNotWall(cube.x, cube.y) && isNotWall(cube.x + SIZE, cube.y))
                        cube.y -= SPEED * Gdx.graphics.getDeltaTime();
                }
            }
            if (y >= cube.y - 10 && y <= cube.y + SIZE + 10) {
                if (x > cube.x) {
                    if (isNotWall(cube.x + SIZE, cube.y) && isNotWall(cube.x + SIZE, cube.y + SIZE))
                        cube.x += SPEED * Gdx.graphics.getDeltaTime();
                } else {
                    if (isNotWall(cube.x, cube.y) && isNotWall(cube.x, cube.y + SIZE))
                        cube.x -= SPEED * Gdx.graphics.getDeltaTime();
                }
            }
        }
    }

    private boolean isNotWall(float x, float y) {
        for (int i = 0; i < walls.size(); i++) {
            if ((int) walls.get(i).getX() / 50 == (int) x / 50 && (int) walls.get(i).getY() / 50 == (int) y / 50) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        batch.draw(lightImg, light.x, light.y);
        batch.draw(cubeImg, cube.x, cube.y);
        drawMaze();
        batch.end();
        music.play();
        InputAn();
        inputKey();
        light.x = cube.x - 250;
        light.y = cube.y - 250;
        limits();
        if ((int) finish.x / 50 == (int) cube.x / 50 && (int) cube.y / 50 == (int) finish.y / 50) {
            music.stop();

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            game.setScreen(new EndScreen(game));
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        cubeImg.dispose();
        emptyImg.dispose();
        wallImg.dispose();
        music.dispose();
        lightImg.dispose();
    }

    private void drawMaze() {
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                map[i][j].draw(batch, i * 50, j * 50);
            }
        }
    }

    private void fillMap() {
        for (int i = 0; i < maze.length; i++) {
            for (int j = 0; j < maze[i].length; j++) {
                if (maze[i][j] == 1) {
                    map[i][j] = new Cell(wallImg, i, j);
                    walls.add(new Cell(wallImg, i * 50, j * 50));
                }
                if (maze[i][j] == 2) {
                    map[i][j] = new Cell(emptyImg);
                    cube = new Rectangle();
                    light = new Rectangle();
                    cube.x = i * 50;
                    cube.y = j * 50;
                    light.x = cube.x - 250;
                    light.y = cube.y - 250;
                }
                if (maze[i][j] == 0) {
                    map[i][j] = new Cell(emptyImg);
                }
                if (maze[i][j] == 3) {
                    map[i][j] = new Cell(emptyImg);
                    finish = new Rectangle();
                    finish.x = i * 50;
                    finish.y = j * 50;
                }
            }
        }
    }
}