package com.mygdx.game;

public class MazeGenerator {
    int[][] maze;
    int empty = 0;
    int[][] getMaze() {
        maze = new int[][]{{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                {1, getNumber(), getNumber(), getNumber(), getNumber(), getNumber(), getNumber(), getNumber(), getNumber(), getNumber(), getNumber(), getNumber(), 1},
                {1, getNumber(), getNumber(), getNumber(), getNumber(), getNumber(), getNumber(), getNumber(), getNumber(), getNumber(), getNumber(), getNumber(), 1},
                {1, empty, empty, empty, getNumber(), getNumber(), getNumber(), getNumber(), empty, empty, empty, empty, 3},
                {1, empty, getNumber(), empty, getNumber(), getNumber(), getNumber(), getNumber(), empty, getNumber(), getNumber(), getNumber(), 1},
                {2, empty, getNumber(), empty, getNumber(), getNumber(), getNumber(), empty, empty, getNumber(), getNumber(), getNumber(), 1},
                {1, getNumber(), getNumber(), empty, getNumber(), getNumber(), getNumber(), empty, getNumber(), getNumber(), getNumber(), getNumber(), 1},
                {1, getNumber(), getNumber(), empty, getNumber(), getNumber(), empty, empty, getNumber(), getNumber(), getNumber(), getNumber(), 1},
                {1, getNumber(), getNumber(), empty, empty, empty, empty, getNumber(), getNumber(), getNumber(), getNumber(), getNumber(), 1},
                {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
        };
        return maze;
    }

    private int getNumber() {
        return (int) (Math.random() * 2);
    }
}
