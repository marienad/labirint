package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Cell {
    Sprite sprite;
    private float x;
    private float y;

    public Cell(Texture texture) {
        sprite = new Sprite(texture);
    }

    public Cell(Texture texture, int x, int y) {
        sprite = new Sprite(texture);
        this.x = x;
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public void draw(SpriteBatch batch, int x, int y) {
        sprite.draw(batch);
        sprite.setPosition(x, y);
    }
}
